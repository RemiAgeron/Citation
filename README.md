Pour lancer le serveur il faut choisir le dossier public.

Le script de la base de donnée, la maquette et le MCD se trouve dans le dossier racine.

Pour avoir accès au backoffice il faut rajouter /login dans l'URL.

Un utilisateur ayant des droits administratifs :

    identifiant : admin-Remi
    mot de passe : coucou
