<?php if (isset($_POST['modify'])) {
  $result = getCitation($conn, $_POST['modify']);
  if ($result) {
    $name = $result['name'];
    $content = $result['text'];
  }
}

if (isset($_POST['citation'])) { 

  if (isset($_POST['author_name']) && isset($_POST['text'])) {

    if ($_POST['citation'] == 'new') {
      
      if (addCitation($conn, $_POST['author_name'], $_POST['text'])) {
        header('Location: .');
      } else { ?>
        <script>alert("La publication a échoué")</script>
      <?php }

    }

    if ($_POST['citation'] == 'modify') {

      if (modifyCitation($conn, $_POST['author_name'], $_POST['text'], $_POST['citation_id'])) {
          header('Location: .');
        } else { ?>
        <script>alert("La modification a échoué")</script>
      <?php }

    }

  } else { ?>
    <script>alert("Veuillez remplir les deux champs de texte s'il vous plaît")</script>
  <?php }

} ?>

<h3><?= isset($_POST['modify']) ? 'Modifier la ' : 'Nouvelle' ?> Citation :</h3>
<form action="." method="POST" class="mini_form">
  <input type="hidden" name="main" value="citation">
  <input type="hidden" name="citation" value=<?= isset($_POST['modify']) ? 'modify' : 'new' ?>>
  <?php if(isset($_POST['modify'])) { ?>
    <input type="hidden" name="citation_id" value=<?= $_POST['modify'] ?>>
  <?php } ?>
  <div>
    <div class="author_name">
      <input type="text" placeholder="Nom de l'auteur" name="author_name" list="authors" value=<?= isset($name) ? $name : '' ?>>
      <datalist id="authors">
        <?php foreach (getAllAuthor($conn) as $key => $value) { ?>
          <option value=<?= $value['name'] ?>>
        <?php } ?>
      </datalist>
      <img src="./import/xmark-solid.svg" alt="Supprimer la sélection" height="30px">
    </div>
    <textarea type="text-area" name="text" placeholder="Écrire la citation ici..." rows="5"><?= isset($content) ? $content : '' ?></textarea>
  </div>
  <h3 class="center">
    <button class="no_button"><?= isset($_POST['modify']) ? 'Modifier' : 'Publier' ?></button>
  </h3>
</form>