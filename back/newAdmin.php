<?php
if (isset($_POST['id']) && isset($_POST['password']) && isset($_POST['key'])) {
  $password = openssl_encrypt($_POST['password'], "AES-128-ECB", $_POST['key']);
  echo NewAdmin($conn, $_POST['id'], $password, $_POST['key']);
}
?>

<h3>Inscription au BackOffice :</h3>
<form action="./backoffice" method="POST" class="center mini_form">
  <input type="text" name="id" maxlength="20" placeholder="identifiant">
  <input type="password" name="password" maxlength="20" placeholder="mot de passe">
  <input type="hidden" name="key" value="<?php
    echo substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(20/strlen($x)) )),1,20);
  ?>">
  <h3>
    <button class="no_button">S'Inscrire</button>
  </h3>
</form>