<?php

function getAllAuthor($conn) {
  $sql = "SELECT * FROM `authors`";
  return $conn->query($sql)->fetch_all(MYSQLI_ASSOC);
}

function getCitation($conn, $id) {
  $sql = "SELECT * FROM `citations` JOIN `authors` USING (`author_id`) WHERE `citation_id` = \"$id\"";
  $result = $conn->query($sql);
  if ($result->num_rows == 0) {
    return false;
  } else {
    return $result->fetch_array(MYSQLI_ASSOC);
  }
}

function getLastCitation($conn) {
  $sql = "SELECT * FROM `citations` JOIN `authors` USING (`author_id`) ORDER BY `date` DESC LIMIT 1";
  return $conn->query($sql)->fetch_array(MYSQLI_ASSOC);
}

function getAllCitation($conn) {
  $sql = "SELECT * FROM `citations` JOIN `authors` USING (`author_id`)";
  return $conn->query($sql)->fetch_all(MYSQLI_ASSOC);
}

function getAuthorCitation($conn, $id) {
  $sql = "SELECT * FROM `citations` JOIN `authors` USING (`author_id`) WHERE `author_id` = \"$id\"";
  return $conn->query($sql)->fetch_all(MYSQLI_ASSOC);
}

function addCitation($conn, $post) {
  $sql = "INSERT INTO `citations` (`author_id`,`text`) VALUES (\"$post[author_id]\", \"$post[text]\")";
  return $conn->query($sql);
}

function newAdmin($conn, $user_id, $password, $key) {
  $sql = "INSERT INTO `backoffice` (`user_id`,`password`, `pass_key`) VALUES (\"$user_id\", \"$password\", \"$key\")";
  return $conn->query($sql);
}

function logIn($conn, $user_id, $password) {
  $sql = "SELECT * FROM `backoffice` WHERE `user_id` = \"$user_id\"";
  $result = $conn->query($sql);
  if ($result->num_rows == 0) {
    return false;
  } else {
    $res = $result->fetch_array(MYSQLI_ASSOC);
    $encrypt_pass = openssl_encrypt($password, "AES-128-ECB", $res['pass_key']);
    if ($res['password'] === $encrypt_pass) {
      return false;
    }
  }
  return true;
}