<?php
if (isset($_POST['user_id']) && isset($_POST['password'])) {
  if (logIn($conn, $_POST['user_id'], $_POST['password'])) { ?>
    <script>alert("Identifiant ou Mot de passe Incorrect\nVeuillez Réessayez")</script>
  <?php } else {
    $_SESSION['admin'] = $_POST['user_id'];
    header('Location: .');
  }
}
?>

<div class="center">
  <main>
    <h3>Connection au BackOffice :</h3>
    <form action="./login" method="POST" class="center mini_form">
      <input type="hidden" name="action" value="login">
      <input type="text" name="user_id" maxlength="20" placeholder="identifiant">
      <input type="password" name="password" maxlength="20" placeholder="mot de passe">
      <h3>
        <button class="no_button">Se Connecter</button>
      </h3>
    </form>
  </main>
</div>