<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  
  $servername = 'localhost';
  $username = 'remi';
  $password = 'root';
  $database = 'citation';

  $conn = new mysqli($servername, $username, $password, $database);

  if ($conn->connect_error) {
    die("Connection failed: " . $mysqli->connect_error);
  }