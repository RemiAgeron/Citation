<?php $author_citation = getAuthorCitation($conn, $_POST['author_id']); ?>

<div class="author_title">
  <h3>Toutes les Citations de :</h3>
  <div class="citation_top">
    <h3><?= $author_citation[0]['name'] ?></h3>
  </div>
</div>

<?php foreach ($author_citation as $key => $value) { ?>
  <div class="citation">
    <form action="." method="POST" class="citation_top">
      <input type="hidden" name="citation" value=<?= $value['citation_id'] ?>>
      <input type="hidden" name="author_id" value=<?= $value['author_id'] ?>>
      <h1><?= substr($value['name'], 0, 1) ?></h1>
      <h3><button name="main" class="no_button" value="author"><?= $value['name'] ?></button></h3>
      <h4><?= (new DateTime($value['date']))->format('d/m/Y H:i') ?></h4>
    </form>
    <p><?= $value['text'] ?></p>
  </div>
<?php } ?>