<?php
  $list = new stdClass();
  if (isset($_SESSION['admin'])) {
    $list->manageCitations = 'gérer les citations';
    $list->manageAuthors = 'gérer les auteurs';
    $list->citation = 'écrire une nouvelle citation';
    $list->logout = 'se déconnecter';
  } else {
    $list->allCitations = 'voir Toutes les citations';
    $list->allAuthors = 'voir Tous les auteurs';
  }
?>

<header>
  <form action="." method="POST">
    <div class="center">
      <div class="row">
        <h1>
          <button name="main" class="no_button" value="main">Citation</button>
        </h1>
        <img src="./import/logo-citation.png" alt="Logo de citation" height="50px">
        <button type="button" class="no_button drop_button" onclick="dropMenu()">
          <img id="menu-open" src="./import/bars-solid.svg" alt="ouvrir menu burger" width="25px">
          <img id="menu-close" class="hide" src="./import/xmark-solid.svg" alt="fermer menu burger" width="25px">
        </button>
      </div>
    </div>
    <div id="drop-down" class="menu">
      <hr>
      <div class="navbar">
        <?php foreach ($list as $element => $value) { ?>
          <h2>
            <button name="main" class="no_button" value=<?= $element ?> style="text-decoration:<?php
              if (isset($_POST['main'])) { if($_POST['main'] == $element) echo 'underline'; }
            ?>"><?= $value?></button>
          </h2>
        <?php } ?>
      </div>
    </div>
  </form>
</header>