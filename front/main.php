<div class="center">
  <main>
    <?php if (isset($_SESSION['admin'])) {

      if (isset($_POST['main'])) {

        switch ($_POST['main']) {

          case 'manageCitations':
            require('../back/manageCitations.php');
            break;

          case 'manageAuthors':
            require('../back/manageAuthors.php');
            break;

          case 'citation':
            require('../back/citation.php');
            break;

          case 'logout':
            session_unset();
            header('Location: .');
            break;

          default:
            require('../front/homepage.php');
            break;

        }

      } else {
        require('../front/homepage.php');
      }

    } else {

          if (isset($_POST['main'])) {

        switch ($_POST['main']) {

          case 'allCitations':
            require('../front/allCitations.php');
            break;

          case 'allAuthors':
            require('../front/allAuthors.php');
            break;

          case 'author':
            require('../front/author.php');
            break;

          default:
            require('../front/homepage.php');
            break;

        }

      } else {
        require('../front/homepage.php');
      }

    } ?>
  </main>
</div>