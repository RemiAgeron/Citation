<h3>Dernière Citation :</h3>

<?php $result = getLastCitation($conn); ?>

<div class="citation">
  <form action="." method="POST" class="citation_top">
    <input type="hidden" name="citation" value=<?= $result['citation_id'] ?>>
    <input type="hidden" name="author_id" value=<?= $result['author_id'] ?>>
    <h1 class="open1"><?= substr($result['name'], 0, 1) ?></h1>
    <h3 class="open2"><button name="main" class="no_button" value="author"><?= $result['name'] ?></button></h3>
    <h4 class="open3"><?= (new DateTime($result['date']))->format('d/m/Y H:i') ?></h4>
  </form>
  <p class="open4"><?= $result['text'] ?></p>
</div>