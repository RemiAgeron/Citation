<h3>Tous les Auteurs :</h3>

<?php foreach (getAllAuthor($conn) as $key => $value) { ?>
  <form action="." method="POST" class="citation_top">
    <input type="hidden" name="author_id" value=<?= $value['author_id'] ?>>
    <h1><?= substr($value['name'], 0, 1) ?></h1>
    <h3><button name="main" class="no_button" value="author"><?= $value['name'] ?></button></h3>
  </form>
<?php } ?>