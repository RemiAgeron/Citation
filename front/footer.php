<footer>
  <?php if (isset($_POST['main'])) {
    if ($_POST['main'] == 'allCitations' || $_POST['main'] == 'allAuthors' || $_POST['main'] == 'author') { ?>
      <div class="center">
        <div class="pagination">
          <h3>
            <button id="previous-page" class="no_button left" onclick="changePage(-1)" disabled>Précédent</button>
            - <span id="count-page">1</span> -
            <button id="next-page" class="no_button right" onclick="changePage(1)">Suivant</button>
          </h3>
        </div>
      </div>
    <?php }
  } ?>
  <p>©copyright 2022 citation.fr</p>
</footer>