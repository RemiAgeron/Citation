<h3>Toutes les Citations :</h3>

<?php foreach (getAllCitation($conn) as $key => $value) { ?>
  <div class="citation">
    <form action="." method="POST" class="citation_top">
      <input type="hidden" name="citation" value=<?= $value['citation_id'] ?>>
      <input type="hidden" name="author_id" value=<?= $value['author_id'] ?>>
      <h1><?= substr($value['name'], 0, 1) ?></h1>
      <h3><button name="main" class="no_button" value="author"><?= $value['name'] ?></button></h3>
      <h4><?= (new DateTime($value['date']))->format('d/m/Y H:i') ?></h4>
    </form>
    <p><?= $value['text'] ?></p>
  </div>
<?php } ?>

<!-- <div class="citation">
  <form action="." method="POST" class="citation_top">
    <input type="hidden" name="author" value="">
    <h1>J</h1>
    <h3><button name="main" class="no_button" value="author">John Doe</button></h3>
    <h4>21/02/22 11:07</h4>
  </form>
  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae accusantium alias aliquam dolore architecto, delectus expedita numquam adipisci, soluta praesentium aut, possimus rerum quaerat culpa asperiores placeat quidem ullam eaque?</p>
</div> -->