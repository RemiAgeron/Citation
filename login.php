<?php

if (isset($_POST['username']) && isset($_POST['password'])) {

  if (CheckPass(
    $conn,
    $_POST['username'],
    openssl_encrypt($_POST['password'], "AES-128-ECB", $_POST['username'])
  )->num_rows == 1) {

    $_SESSION['user'] = $_POST['username'];
    header("Location: .");

  } else { ?>

    <p style="color: red">Connexion refusée</p>

  <?php }

}

if (isset($_SESSION['user'])) { ?>

  <div class="center">
    <button>Déconnexion</button>
  </div>

<?php } else { ?>

  <div class="center">
    <input type="text" id="username" placeholder="pseudonyme">
    <input type="password" id="password" placeholder="mot de passe">
    <button id="button-login">Connexion</button>
  </div>

<?php } ?>

<script>
  document.getElementById('button-login').onclick = () => console.log('coucou')
</script>