<?php
session_start();
require('../../back/connection.php');
require('../../back/crud.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="./../import/logo-citation.png"/>
    <link href="./../style.css" rel="stylesheet">
    <title>Citation - BackOffice</title>
  </head>
  <body>
    <header>
      <form action="." method="POST">
        <div class="center">
          <div class="row">
            <h1>
              <button name="main" class="no_button" value="main">Citation</button>
            </h1>
            <img src="./import/logo-citation.png" alt="Logo de citation" height="50px">
          </div>
        </div>
      </form>
    </header>

    <?php require('../../back/login.php');
    require('../../front/footer.php'); ?>

  </body>
</html>