<?php
session_start();
require('../back/connection.php');
require('../back/crud.php');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="./import/logo-citation.png"/>
    <link href="./style.css" rel="stylesheet">
    <title><?= isset($_SESSION['user']) ? 'Citation - BackOffice' : 'Citation'?></title>
  </head>
  <body>
    <?php
      require('../front/header.php');
      require('../front/main.php');
      require('../front/footer.php');
    ?>
    <script src="./script.js"></script>
  </body>
</html>