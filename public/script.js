
// if (!window.getComputedStyle(openMenu, null).display === 'none') {
// }
const openMenu = document.getElementById('menu-open'),
closeMenu = document.getElementById('menu-close'),
dropDown = document.getElementById('drop-down'),
previous = document.getElementById('previous-page'),
count = document.getElementById('count-page'),
next = document.getElementById('next-page')


let page = 1, max = 5

function dropMenu() {
  if (openMenu.hidden) {
    openMenu.hidden = null
    closeMenu.style.display = null
    dropDown.style.display = null
  } else {
    openMenu.hidden = true
    closeMenu.style.display = 'initial'
    dropDown.style.display = 'inherit'
  }    
}

function changePage(input) {
  page += input
  count.textContent = page

  if (page <= 1) previous.disabled = true
  else previous.disabled = false

  if (page >= max) next.disabled = true
  else next.disabled = false
}